<!-- Begin Bottom -->
	<section class="bottom" data-wow-delay="0.5s">
		<div class="row">
			<div class="small-12 columns">
				<?php dynamic_sidebar( 'bottom' ); ?>
			</div>
		</div>
		<?php get_template_part( 'part', 'copyright' ); ?>
	</section>
<!-- End Bottom -->